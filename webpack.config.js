var Encore = require('@symfony/webpack-encore');

// ====================================================== //
// SET THE DEVELOPMENT SERVER TYPE
// ------------------------------------------------------ //
// This controls only the build process for Apache and
// cannot override whether or not a PHP server is actually
// listening.
//
//       true:      use the PHP-CLI server
//       false:     use Apache to serve files
// ====================================================== //

var usePHPServer = true;

// ====================================================== //
// DESIGNATE THE PROJECT DIRECTORY FOR APACHE ALIASES
// ------------------------------------------------------ //
// This is ignored when building for the PHP-CLI server.
//
// Designate the project root when using Apache directory
// aliases. This is necessary for Encore's 'enableVersioning()'
// to map assets correctly in the produced manifest.json file.
// Note that this is not needed when the web and project roots
// are the same.
//
// When set to 'true', a web directory prefix is assigned in
// the manifest file, using 'projectDirectory' below.
// 'projectDirectory' should match the Apache-configured alias
//
// Reference: https://github.com/symfony/webpack-encore/issues/26
//
//      true:       use aliased directories
//      false:      the web root and project root are the same
//
// ====================================================== //

var useApacheAliases    = true;
var apacheAlias         = 'campus-tours';

// ====================================================== //
// CONFIGURE ENCORE
// ====================================================== //

// set the public path for storing assets
var publicPath = 'assets';

// control prefixing for the public path when using Apache
if (!usePHPServer) {
    console.log('Building for Apache server...');
    if (useApacheAliases && 'undefined' != typeof apacheAlias) {
        Encore.setManifestKeyPrefix(publicPath);
        console.log('Using directory aliases with the following path to map assets: ' + publicPath);
    }
} else {
    console.log('Building for PHP-CLI server...');
    var encorePublicPath = 'public';
    var assetDirectory = 'assets';
    var encoreOutputPath = encorePublicPath + '/' + assetDirectory;
}

Encore
    .setPublicPath('/' + assetDirectory)
    .setOutputPath(encoreOutputPath)
    .cleanupOutputBeforeBuild()
    // .enableSourceMaps(!Encore.isProduction())
    // .enableVersioning()
    .enableSassLoader()

    .addEntry('js/app', './assets/js/app.js')
    .addStyleEntry('css/app', './assets/css/app.scss')
    .configureFilenames({
        images: 'images/[name].[ext]',
    })
;

module.exports = [ Encore.getWebpackConfig() ];
