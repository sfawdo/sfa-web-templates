require('./static-assets.js');

// == FLASHES ====
if (document.getElementById("flash-msg")) {
  setTimeout(function(){
    document.getElementById("flash-msg").classList.add('slideOutUp');
  }, 5000);
}
