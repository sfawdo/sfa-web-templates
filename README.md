# SFA web templates

This is a basic set of CSS styles that provide a degree of consistency across all SFASU websites and web applications.

This is a work in progress.

## Downloads

[Latest version](https://bitbucket.org/sfawdo/sfa-web-templates/get/master.zip)

## Usage

Copy the contents of the `public/` directory into your project and use the provided HTML files (`base.html` and `example.html`) to see examples.

There is an empty `custom.css` file in the `public/` directory for you to add any custom CSS styling you need.

Save any project-specific images in the `public/images/` directory.

Do not modify, delete or add files in the `public/assets/` directory.
